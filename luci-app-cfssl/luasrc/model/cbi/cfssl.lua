
local packageName = "cfssl"
local uci = require "luci.model.uci".cursor()
local sys = require "luci.sys"
local util = require "luci.util"
local ip = require "luci.ip"
local fs = require "nixio.fs"
local jsonc = require "luci.jsonc"
local http = require "luci.http"
local nutil = require "nixio.util"
local dispatcher = require "luci.dispatcher"
local enabledFlag = uci:get(packageName, "config", "enabled")
local enc


local pkgVersion = tostring(util.trim(sys.exec("opkg list-installed " .. packageName .. " | awk '{print $3}'")))
if not pkgVersion or pkgVersion == "" then
	pkgVersion = ""
	pkgStatus, pkgStatusLabel = "NotFound", packageName .. " " .. translate("is not installed or not found")
else  
	pkgVersion = " [" .. packageName .. " " .. pkgVersion .. "]"
end
local pkgStatus, pkgStatusLabel = "Stopped", translate("Stopped")
if sys.call("pidof cfssl >/dev/null") == 0 then
	pkgStatus, pkgStatusLabel = "Running", translate("Running")
end

local lanIPAddr = uci:get("network", "lan", "ipaddr")
-- if multiple ip addresses on lan interface, will be return as table of CIDR notations i.e. {"10.0.0.1/24","10.0.0.2/24"}
if (type(lanIPAddr) == "table") then
				first = true
				for i,line in ipairs(lanIPAddr) do
								lanIPAddr = lanIPAddr[i]
								break
				end
				lanIPAddr = string.match(lanIPAddr,"[0-9.]+")
end          
if lanIPAddr and lanNetmask then
	laPlaceholder = ip.new(lanIPAddr .. "/" .. lanNetmask )
end

function is_wan(name)
	return name:sub(1,3) == "wan" or name:sub(-3) == "wan"
end


m = Map("cfssl", translate("CFSSL Cloudflare's PKI and TLS toolkit"))

h = m:section(NamedSection, "config", packageName, translate("Service Status") .. pkgVersion)
status = h:option(DummyValue, "_dummy", translate("Service Status"))
status.template = "cfssl/status"
status.value = pkgStatusLabel
if pkgErrors and pkgErrors ~= "" then
	errors = h:option(DummyValue, "_dummy", translate("Service Errors"))
	errors.template = packageName .. "/status-textarea"
	errors.value = pkgErrors
end
if pkgWarnings and pkgWarnings ~= "" then
	warnings = h:option(DummyValue, "_dummy", translate("Service Warnings"))
	warnings.template = packageName .. "/status-textarea"
	warnings.value = pkgWarnings
end
buttons = h:option(DummyValue, "_dummy")
buttons.template = packageName .. "/buttons"

-- General Options
config = m:section(NamedSection, "config", "cfssl", translate("General"))
config.override_values = true
config.override_depends = true

-- Basic Options
config:tab("basic", translate("Configuration"))

address = config:taboption("basic", Value, "address", translate("Address"), translate("Listen address."))
address.optional = false
address.rmempty = true

port = config:taboption("basic", Value, "port", translate("Port"), translate("Listen port."))
port.optional = false
port.rmempty = true

cn = config:taboption("basic", Value, "common_name", translate("Common Name"), translate("Certificate base Common name."))
cn.optional = false
cn.rmempty = true

ca_expiry = config:taboption("basic", Value, "ca_expiry", translate("CA Expiry"), translate("Certificate default CA expiry."))
ca_expiry.optional = false
ca_expiry.rmempty = true

country = config:taboption("basic", Value, "country", translate("Country"), translate("Certificate default country."))
country.optional = false
country.rmempty = true

location = config:taboption("basic", Value, "location", translate("Location"), translate("Certificate default location."))
location.optional = false
location.rmempty = true

organization = config:taboption("basic", Value, "organization", translate("Organization"), translate("Certificate default organization."))
organization.optional = false
organization.rmempty = true

state = config:taboption("basic", Value, "state", translate("State"), translate("Certificate default state."))
state.optional = false
state.rmempty = true

config:tab("ca", translate("Root CA"))

return m
