module("luci.controller.cfssl", package.seeall)
function index()
	if nixio.fs.access("/etc/config/cfssl") then
		entry({"admin", "services"}, firstchild(), _("CFSSL"), 60).dependent=false
		entry({"admin", "services", "cfssl"}, cbi("cfssl"), _("CFSSL"))
		entry({"admin", "services", "cfssl", "action"}, call("cfssl_action"), nil).leaf = true
	end
end

function cfssl_action(name)
	local packageName = "cfssl"
	if name == "start" then
		luci.sys.init.start(packageName)
	elseif name == "action" then
		luci.util.exec("/etc/init.d/" .. packageName .. " reload >/dev/null 2>&1")
	elseif name == "stop" then
		luci.sys.init.stop(packageName)
	elseif name == "enable" then
		luci.util.exec("uci set " .. packageName .. ".config.enabled=1; uci commit " .. packageName)
	elseif name == "disable" then
		luci.util.exec("uci set " .. packageName .. ".config.enabled=0; uci commit " .. packageName)
	end
	if name == "download" then
		luci.http.prepare_content("application/x-x509-ca-cert")
		luci.http.write(nixio.fs.readfile("/etc/cfssl/ca/ca.pem"))
	else
		luci.http.prepare_content("text/plain")
		luci.http.write("0")
	end
end
