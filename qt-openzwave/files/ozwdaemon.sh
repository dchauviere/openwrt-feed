#!/bin/sh
[[ -d /var/lib/openzwave ]] || mkdir -p /var/lib/openzwave
cd /var/lib/openzwave
/usr/bin/ozwdaemon $@
