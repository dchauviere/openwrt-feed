#!/bin/sh

CFSSL_CONFDIR=${CONFDIR:-/etc/cfssl}

function gencsr() {
  local cn=$1
  local mode=$2
  local hosts=$3
  local ca_option=""

  case $mode in
  ca)
    ca_option="\"CA\": { \"expiry\": \"${CFSSL_CA_EXPIRY}\", \"pathlen\": 0},"
    ;;
  server)
    hostslist=$( IFS=, ; for h in $hosts; do echo "\"$h\", " ; done )
    host_option="hosts:[${hostslist%,}],"
    ;;
  esac
  cat <<EOF
{
  ${ca_options}
  ${host_option}
  "CN": "${CFSSL_COMMON_NAME}",
  "key": { "algo": "rsa", "size": 2048 },
  "names": [
    {
      "C": "${CFSSL_COUNTRY}",
      "L": "${CFSSL_LOCATION}",
      "O": "${CFSSL_ORGANIZATION}",
      "ST": "${CFSSL_STATE}"
    }
  ]
}
EOF
}

function extract_certs {
  local file=$1
  shift
  local elem=$@
  local dir=$(dirname $file)
  local filename=$(basename $file .json)

  for e in $elem; do
    case $e in
    cert)
      cat "$file" | jq -r .cert > "${dir}/${filename}.pem" 
      ;;
    csr)
      cat "$file" | jq -r .csr > "${dir}/${filename}.csr" 
      ;;
    key)
      cat "$file" | jq -r .key > "${dir}/${filename}-key.pem" 
      ;;
    esac
  done
}

function init_config() {

  mkdir -p ${CFSSL_CONFDIR}/ca ${CFSSL_CONFDIR}/intermediate ${CFSSL_CONFDIR}/ocsp
  
  cat > ${CFSSL_CONFDIR}/db.json <<EOF
{"driver":"sqlite3","data_source":"/var/lib/cfssl/certdb.db"}
EOF

  cat > ${CFSSL_CONFDIR}/config.json <<EOF
{
  "signing": {
    "default": {
      "ocsp_url": "https://${CFSSL_ADDRESS}:$(( ${CFSSL_PORT} + 1 ))",
      "crl_url": "https://${CFSSL_ADDRESS}:${CFSSL_PORT}/crl",
      "auth_key": "default",
      "expiry": "${CFSSL_CA_EXPIRY}"
    },
    "profiles": {
      "ocsp": {
        "usages": ["digital signature", "ocsp signing"],
        "expiry": "${CFSSL_CA_EXPIRY}"
      },
      "intermediate": {
        "auth_key": "ca",
        "expiry": "${CFSSL_CA_EXPIRY}",
        "usages": [ "signing", "key encipherment", "cert sign", "crl sign" ],
        "ca_constraint": { "is_ca": true }
      },
      "server": {
        "usages": [ "signing", "key encipherment", "server auth" ],
        "expiry": "${CFSSL_CA_EXPIRY}"
      },
      "client": {
        "usages": [ "signing", "key encipherment", "client auth" ],
        "expiry": "${CFSSL_CA_EXPIRY}"
      },
      "peer": {
        "usages": [ "signing", "key encipherment", "server auth", "client auth" ],
        "expiry": "${CFSSL_CA_EXPIRY}"
      }
    }
  },
  "auth_keys": {
    "default": {
      "key": "$(openssl rand -hex 16)",
      "type": "standard"
    },
    "ca": {
      "key": "$(openssl rand -hex 16)",
      "type": "standard"
    }
  }
}
EOF
}

function init_db() {
  [ -f /var/lib/cfssl/certdb.db ] && return 0
  mkdir -p /var/lib/cfssl
  cat | sqlite3 /var/lib/cfssl/certdb.db <<EOF
CREATE TABLE certificates (
  serial_number            blob NOT NULL,
  authority_key_identifier blob NOT NULL,
  ca_label                 blob,
  status                   blob NOT NULL,
  reason                   int,
  expiry                   timestamp,
  revoked_at               timestamp,
  pem                      blob NOT NULL,
  PRIMARY KEY(serial_number, authority_key_identifier)
);

CREATE TABLE ocsp_responses (
  serial_number            blob NOT NULL,
  authority_key_identifier blob NOT NULL,
  body                     blob NOT NULL,
  expiry                   timestamp,
  PRIMARY KEY(serial_number, authority_key_identifier),
  FOREIGN KEY(serial_number, authority_key_identifier) REFERENCES certificates(serial_number, authority_key_identifier)
);
EOF
}

function generate_certs() {
  if [ ! -f "${CFSSL_CONFDIR}/ca/ca.pem" ]; then
    gencsr "${CFSSL_COMMON_NAME} root CA" "ca" | \
      cfssl gencert -config "${CFSSL_CONFDIR}/config.json" -initca - \
        > "${CFSSL_CONFDIR}/ca/ca.json"
    extract_certs "${CFSSL_CONFDIR}/ca/ca.json" cert csr key
  fi

  if [ ! -f "${CFSSL_CONFDIR}/ca-server.pem" ]; then
    gencsr "${CFSSL_COMMON_NAME} intermediate CA" | \
      cfssl gencert -ca "${CFSSL_CONFDIR}/ca/ca.pem" -ca-key "${CFSSL_CONFDIR}/ca/ca-key.pem" --config "${CFSSL_CONFDIR}/config.json" -profile intermediate - \
        > "${CFSSL_CONFDIR}/ca-server.json"
    extract_certs "${CFSSL_CONFDIR}/ca-server.json" cert csr key

    gencsr "${CFSSL_COMMON_NAME} OCSP CA" | \
      cfssl gencert -ca "${CFSSL_CONFDIR}/ca-server.pem" -ca-key "${CFSSL_CONFDIR}/ca-server-key.pem" --config "${CFSSL_CONFDIR}/config.json" -profile ocsp - \
        > "${CFSSL_CONFDIR}/ocsp.json"
    extract_certs "${CFSSL_CONFDIR}/ocsp.json" cert csr key

    gencsr "$HOSTNAME" hosts "$HOSTNAME" | \
      cfssl gencert -ca "${CFSSL_CONFDIR}/ca-server.pem" -ca-key "${CFSSL_CONFDIR}/ca-server-key.pem" --config "${CFSSL_CONFDIR}/config.json" -profile server - \
        > "${CFSSL_CONFDIR}/server.json"
    extract_certs "${CFSSL_CONFDIR}/server.json" cert csr key
  fi
}
