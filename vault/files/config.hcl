storage "file" {
  path    = "/var/lib/vault"
}

listener "tcp" {
  address = "127.0.0.1:8200"
  tls_cert_file = "/etc/vault/vault.crt"
  tls_key_file = "/etc/vault/vault.key"
  tls_disable = 0
}

telemetry {
  prometheus_retention_time = "24h"
  disable_hostname = true
}